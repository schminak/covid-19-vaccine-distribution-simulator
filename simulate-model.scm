#!/usr/bin/guile -s
!#

(use-modules (ice-9 getopt-long) (ice-9 hash-table) (ice-9 pretty-print) (ice-9 match)
	     (srfi srfi-1) (srfi srfi-11))

(load "csv-parse.scm")
(load  "alist-db.scm")

(define (simulate-model)
  ;; Default initial conditions.
  (define default-time-step  0.0002)
  (define default-simulation-iterations 5000)
  (define default-time-infectious 14)
  (define (default-vaccination-rate age susceptible-of-age) 0)
  (define (default-mu age) (/ 4 1400))
  (define default-basic-reproduction-number 1.11)

  ;; Print a help message which explains this programs usage.
  (define (help-opt)
    (display (string-append
	      "\t-h, --help\tDisplay this help message.\n"
	      "\t-c, --input-config\tRead in a specified initial conditions config file.\n"
	      "\t-v, --input-config-vaccine\tRead in a specified vaccine config file.\n"
	      "\t-s, --input-state\tRead in a specified state file.\n"
	      "\t-p, --out-print\tPretty-print simulation output S-Expression to stdout.\n"
	      "\t-w, --out-write-csv\tWrite simulation output as CSV to stdout.\n"
	      "\t-x, --out-write-scm\tWrite simulation output as SCM data to stdout.\n"
	      "\t-f, --out-file-csv\tWrite simulation output as CSV to the specified file name.\n"
	      "\t-g, --out-file-scm\tWrite simulation output as SCM data to the specified file name.\n"
	      "\t-l, --out-file-csv-last\tWrite simulations final state as CSV to the specified file name.\n")))

  ;; Define and parse command line options, then implement and run simulation.
  ;; Command line options are defined and parsed within the let* bindings.
  (let* ((option-spec
	  '((help                 (single-char #\h) (value #f))
	    (input-config         (single-char #\c) (value #t))
	    (input-config-vaccine (single-char #\v) (value #t))
	    (input-state          (single-char #\s) (value #t))
	    (out-print            (single-char #\p) (value #f))
	    (out-write-csv        (single-char #\w) (value #f))
	    (out-write-scm        (single-char #\x) (value #f))
	    (out-file-csv         (single-char #\f) (value #t))
	    (out-file-scm         (single-char #\g) (value #t))
	    (out-file-csv-last    (single-char #\l) (value #t))))
	 (options                   (getopt-long (command-line) option-spec))
	 (flag-help                 (option-ref options 'help                 #f))
	 (flag-input-state          (option-ref options 'input-state          #f))
	 (flag-input-config         (option-ref options 'input-config         #f))
	 (flag-input-config-vaccine (option-ref options 'input-config-vaccine #f))
	 (flag-out-print            (option-ref options 'out-print            #f))
	 (flag-out-write-csv        (option-ref options 'out-write-csv        #f))
	 (flag-out-write-scm        (option-ref options 'out-write-scm        #f))
	 (flag-out-file-csv         (option-ref options 'out-file-csv         #f))
	 (flag-out-file-scm         (option-ref options 'out-file-scm         #f))
	 (flag-out-file-csv-last    (option-ref options 'out-file-csv-last    #f))
	 (initial-state (if flag-input-state (call-with-input-file flag-input-state read) #f))
	 (pop-by-age (call-with-input-file "data/population-by-age.scm" read))
	 (compartments '(susceptible vaccinated recovered infectious dead))
	 (ages '("0 - 9 Years"   "10 - 19 Years" "20 - 29 Years" "30 - 39 Years" "40 - 49 Years"
		 "50 - 59 Years" "60 - 69 Years" "70 - 79 Years" "80+ Years")))

    ;; Read in initial conditions from a configuration file, if one was input.
    (define config-list
      (if flag-input-config (with-input-from-file flag-input-config read) '()))

    ;; Read in the vaccination rate by age from a vaccine-config file, if specified.
    (define vaccine-config
      (if flag-input-config-vaccine (with-input-from-file flag-input-config-vaccine read) '()))

    ;; Return initial condition if defined in config file
    (define (find-condition condition default-condition)
      (let ((condition-entry (assq condition config-list)))
	(if condition-entry (cdr condition-entry) default-condition)))

    ;; Initial conditions as used in the rest of the program.
    ;; time-step: a.k.a the ΔT.
    (define time-step
      (find-condition 'time-step default-time-step))

    (define simulation-iterations
      (find-condition 'simulation-iterations default-simulation-iterations))

    ;; time-infectious (Τ): period for which a sick person can infect others.
    ;; Value read from the general config file, if present. Otherwise it
    ;; is set to the value of default-time-infectious, as defined above.
    (define time-infectious
      (find-condition 'time-infectious default-time-infectious))

    ;; vaccination-rate: vaccine doses distributed per day, by age group.
    ;; Value is read from specified vaccine-config file,
    ;; or from a general config file, if the former is not present.
    ;; If no config files are present, then the value is set to that of
    ;; default-vaccination-rate, as defined above.
    (define vaccination-rate
      (let ((vaccine-from-config (assq 'vaccination-rate config-list)))
	(cond ((not (null? vaccine-config))
	       (lambda (age susceptible-of-age)
		 (if (< 1 susceptible-of-age)
		     (cdr (assoc age (cdr vaccine-config)))
		     0.0)))
	      (vaccine-from-config
	       (lambda (age susceptible-of-age)
		 (if (< 1 susceptible-of-age)
		     (cdr (assoc age (cdr vaccine-from-config)))
		     0.0)))
	      (else default-vaccination-rate))))

    ;; μ: the rate of mortality among those who are sick.
    (define mu
      (let ((mu-from-config (assq 'mu config-list)))
	(if mu-from-config
	    (lambda (age) (cdr (assoc age (cdr mu-from-config))))
	    default-mu)))

    ;; basic-reproduction-number (R_t): the average number of people who get
    ;; infected from a sick person over the course of their illness.
    (define basic-reproduction-number
      (find-condition 'basic-reproduction-number default-basic-reproduction-number))

    ;; ɣ: rate of recovery.
    (define gamma (/ 1 time-infectious))

    ;; β: rate of infection events. Such events, when they occur between
    ;; an infected and a susceptible individual, will cause and infection.
    (define beta (* basic-reproduction-number gamma))

    ;; Return all compartments of type compartment-type from state (a list of compartments).
    (define (filter-compartment state compartment-type)
      (filter (lambda (c) (eq? compartment-type (car c))) state))

    ;; Return all compartments in state except those of type compartment-type.
    (define (remove-compartment state compartment-type)
      (filter (lambda (c) (not (eq? compartment-type (car c)))) state))

    ;; Return all compartments for the age group age-type in state.
    (define (filter-age state age-type)
      (filter (lambda (c) (equal? age-type (cadr c))) state))

    ;; Returns the sum of people in all compartments in state.
    (define (total-in-state state) (apply + (map caddr state)))

    ;; Returns the number of people from the compartments
    ;; in state with the corresponding compartment and age.
    (define (total-in-group state compartment age)
      (total-in-state (filter-age (filter-compartment state compartment) age)))

    (define (age-total age state) (total-in-state (filter-age state age)))

    ;; Total population size of a given age.
    (define (age-pop age) (assoc-ref pop-by-age age))

    ;; Takes a given state and returns the state for the next time-step.
    (define (model-increment state)
      (let ((all-infectious (total-in-state (filter-compartment state 'infectious)))
	    (population     (total-in-state (remove-compartment state       'dead))))

	;; Return the size of the compartment matching both compartment
	;; and age after it is incremented by one time step.
	(define (next-total compartment age)
	  (let ((total           (total-in-group state  compartment age))
		(age-susceptible (total-in-group state 'susceptible age))
		(age-infectious  (total-in-group state  'infectious age)))
	    (match compartment
		   ('susceptible
		    (- age-susceptible
		       (/ (* 1.0 beta all-infectious age-susceptible time-step) population)
		       (* (vaccination-rate age age-susceptible) time-step)))
		   ('vaccinated  (+ total (* 1.0 (vaccination-rate age age-susceptible) time-step)))
		   ('recovered   (+ total (* 1.0 gamma age-infectious time-step)))
		   ('infectious
		    (+ total
		       (- (/ (* 1.0 beta all-infectious age-susceptible time-step) population)
			  (* gamma age-infectious time-step)
			  (* (mu age) age-infectious time-step))))
		   ('dead (+ total (* 1.0 (mu age) age-infectious time-step))))))
	(append-map (lambda (c) (map (lambda (a) (list c a (next-total c a))) ages)) compartments)))

    ;; Iterates for each time-step and annotates each state with the corresponding time.
    ;; Returns a list containing all time-annotated states.
    (define (run-model init-state)
      (define (run-model-aux time step-number state state-frames)
	(cond ((> step-number simulation-iterations) (cons (cons time state) state-frames))
	      (else (run-model-aux (+ time time-step)
				   (1+ step-number)
				   (model-increment state)
				   (cons (cons time state) state-frames)))))
      (run-model-aux 0 0 init-state '()))

    ;; Prints help message if the help flag was passed to this program.
    (when flag-help (help-opt))

    ;; Runs simulation and outputs result as specified by user.
    (when initial-state
      (let ((model (run-model initial-state)))
	(when flag-out-print     (pretty-print              model))
	(when flag-out-write-csv (display      (model->csv model)))
	(when flag-out-write-scm (write                     model))
	(when flag-out-file-csv
	  (with-output-to-file flag-out-file-csv
	    (lambda () (display (model->csv model)))))
	(when flag-out-file-scm
	  (with-output-to-file flag-out-file-scm
	    (lambda () (write model))))
	(when flag-out-file-csv-last
	  (with-output-to-file flag-out-file-csv-last
	    (lambda () (display (model->csv (list (car model)))))))))))

(simulate-model)
