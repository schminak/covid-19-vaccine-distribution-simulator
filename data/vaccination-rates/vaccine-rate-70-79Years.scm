(vaccination-rate
 ;; vaccination-rate: number of vaccines distributed per day by age group.
 ("70 - 79 Years" . 36000)
 ("0 - 9 Years" . 0)
 ("10 - 19 Years" . 0)
 ("20 - 29 Years" . 0)
 ("30 - 39 Years" . 0)
 ("40 - 49 Years" . 0)
 ("50 - 59 Years" . 0)
 ("60 - 69 Years" . 0)
 ("80+ Years" . 0))
