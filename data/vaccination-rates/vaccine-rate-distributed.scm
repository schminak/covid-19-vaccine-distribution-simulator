(vaccination-rate
 ;; vaccination-rate: number of vaccines distributed per day by age group.
 ("0 - 9 Years"   . 4000.0)
 ("10 - 19 Years" . 4000.0)
 ("20 - 29 Years" . 4000.0)
 ("30 - 39 Years" . 4000.0)
 ("40 - 49 Years" . 4000.0)
 ("50 - 59 Years" . 4000.0)
 ("60 - 69 Years" . 4000.0)
 ("70 - 79 Years" . 4000.0)
 ("80+ Years" .     4000.0))
