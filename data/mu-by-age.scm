((mu
  ;; μ: mortality rate of the infected by age group.
  ;; Values found from Table-1 (Mortality, per 10 Person-Days (PD)) in:
  ;; http://weekly.chinacdc.cn/en/article/doi/10.46234/ccdcw2020.032
  ("0 - 9 Years"   . 0.0)
  ("10 - 19 Years" . 0.0002)
  ("20 - 29 Years" . 0.0001)
  ("30 - 39 Years" . 0.0002)
  ("40 - 49 Years" . 0.0003)
  ("50 - 59 Years" . 0.0009)
  ("60 - 69 Years" . 0.00024)
  ("70 - 79 Years" . 0.0056)
  ("80+ Years"     . 0.0111)))
