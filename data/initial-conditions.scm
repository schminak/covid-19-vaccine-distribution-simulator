;; Example configuration file.
;; Initial conditions are defined as scheme data in an association list.
((time-step . 0.032)
 (simulation-iterations . 2750)
 (time-infectious . 14)
 (vaccination-rate
  ;; vaccination-rate: number of vaccines distributed per day by age group.
  ("0 - 9 Years"   . 0)
  ("10 - 19 Years" . 0)
  ("20 - 29 Years" . 0)
  ("30 - 39 Years" . 0)
  ("40 - 49 Years" . 0)
  ("50 - 59 Years" . 0)
  ("60 - 69 Years" . 0)
  ("70 - 79 Years" . 0)
  ("80+ Years"     . 0))
 (mu
  ;; μ: mortality rate of the infected by age group.
  ;; Values found from Table-1 (Mortality, per 10 Person-Days (PD)) in:
  ;; http://weekly.chinacdc.cn/en/article/doi/10.46234/ccdcw2020.032
  ("0 - 9 Years"   . 0.0)
  ("10 - 19 Years" . 0.0002)
  ("20 - 29 Years" . 0.0001)
  ("30 - 39 Years" . 0.0002)
  ("40 - 49 Years" . 0.0003)
  ("50 - 59 Years" . 0.0009)
  ("60 - 69 Years" . 0.00024)
  ("70 - 79 Years" . 0.0056)
  ("80+ Years"     . 0.0111))
 (basic-reproduction-number . 3.0))
