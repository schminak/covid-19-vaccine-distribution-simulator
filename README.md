A compartmental model for predicting effective vaccine distribution schemes 
================
## simulate-model.scm

### SYNOPSIS

`./simulate-model.scm [OPTION...]`

`./simulate-model.scm -h`

`./simulate-model.scm -c INPUT_CONFIG_FILE -v INPUT_VACCINATION_RATE_FILE -s INPUT_STATE_FILE -f RESULT_CSV_FILE`

### DESCRIPTION

The intention of this program is to model the spread of COVID-19 based on defined initial parameters --- particularly vaccine distribution.

This program is currently under development. Meaningful simulations can be carried out, but user interface and data format changes may be frequent.

### OPTIONS

#### Generic Program Information
 
`-h, --help`
Print summary of options.

#### Data Input

`-c, --input-config INPUT_CONFIG_FILE`
Load initial conditions from `INPUT_CONFIG_FILE` for the next simulation, where `INPUT_CONFIG_FILE` is an association list scheme data. Existing examples of how such data is formatted can be found in the file `data/initial-conditions.scm`.

`-v, --input-config-vaccine INPUT_VACCINATION_RATE_FILE`
Load initial conditions from `INPUT_VACCINATION_RATE_FILE` for the next simulation, where `INPUT_VACCINATION_RATE_FILE` is an association list scheme data. Existing examples of how such data is formatted can be found in the files of `data/vaccination-rates/`.

`-s, --input-state INPUT_STATE_FILE`
Load the initial state of the simulation from `INPUT_STATE_FILE`, where `INPUT_STATE_FILE` is the formatted scheme data output of `convert-cdc.scm` when ran on data as provided by the CDC.
#### Data Output

`-p, --out-print`
Pretty-print the simulations output to standard output as scheme data.

`-w, --out-write-csv`
Write the simulations output as CSV formatted data to standard output.

`-x, --out-write-scm`
Write the simulations output as scheme data to standard output.

`-f, --out-file-csv RESULT_FILENAME`
Write the simulations output to the specified file name `RESULT_FILENAME` as CSV formatted data.

`-g, --out-file-scm RESULT_FILENAME`
Write the simulations output to the specified file name `RESULT_FILENAME` as scheme formatted data.

`-f, --out-file-csv-last RESULT_FILENAME`
Write the last state of the simulation as output to the specified file name `RESULT_FILENAME` as CSV formatted data.

### USING THE PROGRAM

[GNU Guile](https://www.gnu.org/software/guile/) must be installed to run successfully. So far the program has been tested on Guile version 2.2.4 on [Debian 10 "Buster" GNU/Linux](https://www.debian.org/). To use the `execute-simulations.sh` script, [GNU Parallel](https://www.gnu.org/software/parallel/) is also required.

In a shell running in the project directory simply run the command shown below:

`:~/covid-19-vaccine-distribution-simulator$ ./execute-simulations.sh`

to carry out a simulation for each vaccination-rate file present in `data/vaccination-rates/`. The results are then output to the directory `data/simulation-output/` as CSV formatted data. The program `simulate-model.scm` can just as easily be invoked at the shell. It is recommended to look into the `execute-simulations.sh` shell script for an example of its usage.

By default, `execute-simulations.sh` uses the initial state file `data/full-initial-simulation-state.scm`. This was generated using `convert-cdc.scm` from the entirety of the CDC surveillance public use data as of early September, 2020.

## convert-cdc.scm

### SYNOPSIS

`./convert-cdc.scm [OPTION...]`

`./convert-cdc.scm -h`

`./convert-cdc.scm -c CSV_CDC_SURVEILLANCE_PUBLIC_USE_DATA_FILE -f OUTPUT_INITIAL_STATE_FILE`

### DESCRIPTION

Initial conditions can be read in from data sets provided by the CDC: [Surveillance Public Use Data](https://data.cdc.gov/Case-Surveillance/COVID-19-Case-Surveillance-Public-Use-Data/vbim-akqf) (requires formatting --- see examples in test directory) by using the `-c` or `--input-csv` switch with the filename as an argument. Files should be formatted as CSVs and may not have commas within a field (even if the field is enclosed within quotation marks).

### OPTIONS

#### Generic Program Information
 
`-h, --help`
Print summary of options.

#### Data Input

`-c, --input-csv CSV_CDC_DATA-SET_FILENAME`
Read `CSV_CDC_DATA-SET_FILENAME` to generate the initial state for a simulation.

#### Data Output

`-p, --out-print`
Pretty-print the initial state to standard output as scheme data.

`-w, --out-write`
Write the initial state as scheme data to standard output.

`-f, --out-file RESULT_FILENAME`
Write the initial state as scheme data to the specified file name `RESULT_FILENAME`.

### USING THE PROGRAM

[GNU Guile](https://www.gnu.org/software/guile/) must be installed to run successfully. So far the program has been tested on Guile version 2.2.4 on [Debian 10 "Buster" GNU/Linux](https://www.debian.org/).

In a shell running in the project directory simply run the command shown below:

`:~/covid-19-vaccine-distribution-simulator$ ./convert-cdc.scm --input-csv tests/COVID-19_Case_Surveillance_Public_Use_Data-First_50_Entries.csv --out-file tests/example-initial-simulation-state.scm`

to read in and process the example CDC data. The results are then output to the file `example-initial-simulation-state.scm` in the `tests` directory.

For more meaningful results, it is recommended that the newest data set provided from the CDC is used (linked above).

