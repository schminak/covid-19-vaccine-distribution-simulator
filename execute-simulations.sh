#!/usr/bin/bash

parallel ./simulate-model.scm \
	 --input-config-vaccine {} \
	 --input-config data/initial-conditions.scm \
	 --input-state  data/full-initial-simulation-state.scm \
	 --out-file-csv data/simulation-output/{/.}-full-result.csv \
	 ::: data/vaccination-rates/*
