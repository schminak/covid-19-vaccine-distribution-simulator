(use-modules (ice-9 hash-table) (srfi srfi-1))

(define (alist->alist-db al)
  (let ((al-legend (car al))
	(al-entries (cdr al)))
    (define (compress-legend-key-dupes lk)
      (let ((unique-lk-lst (delete-duplicates (map car lk))))
	(map (lambda (key-lookup-str)
	       (cons key-lookup-str
		     (filter-map
		      (lambda (key-str-pair)
			(if (equal? key-lookup-str (car key-str-pair))
			    (cdr key-str-pair) #f))
		      lk)))
	     unique-lk-lst)))
		      
    (define (alist->legend-key-alist legend-param)
      (cons legend-param
	    (compress-legend-key-dupes
	     (map (lambda (al-entry)
		    (cons (assq-ref (cdr al-entry) legend-param)
			  (car al-entry)))
		  al-entries))))
    (cons (map alist->legend-key-alist al-legend) al-entries)))
