#!/usr/bin/guile -s
!#

(use-modules (ice-9 getopt-long) (ice-9 hash-table) (ice-9 pretty-print) (ice-9 match)
	     (srfi srfi-1) (srfi srfi-11))

(load "csv-parse.scm")
(load  "alist-db.scm")

(define (convert-cdc)
  (define (help-opt)
    (display (string-append
	      "\t-h, --help\tDisplay this help message.\n"
	      "\t-c, --input-csv\tRead in a specified CSV data file.\n"
	      "\t-p, --out-print\tPretty-print initial state to stdout.\n"
	      "\t-w, --out-write\tWrite initial state as SCM data to stdout.\n"
	      "\t-f, --out-file\tWrite initial state as SCM data to a specified file name.\n")))
  (let* ((option-spec
	  '((help             (single-char #\h) (value #f))
	    (input-csv        (single-char #\c) (value #t))
	    (input-alist      (single-char #\a) (value #t))
	    (out-print        (single-char #\p) (value #f))
	    (out-write        (single-char #\w) (value #f))
	    (out-file         (single-char #\f) (value #t))))
	 (options (getopt-long (command-line) option-spec))
	 (help-opt-flag    (option-ref options 'help        #f))
	 (input-csv-flag   (option-ref options 'input-csv   #f))
	 (out-print-flag   (option-ref options 'out-print   #f))
	 (out-write-flag   (option-ref options 'out-write   #f))
	 (out-file-flag    (option-ref options 'out-file    #f))
	 (input-db
	  (cond
	   (input-csv-flag (csv->alist-db (call-with-input-file input-csv-flag csv-file->csv-lst)))
	   (else #f))))

    (define pop-by-age (call-with-input-file "data/population-by-age.scm" read))
    (define ages '("0 - 9 Years"   "10 - 19 Years" "20 - 29 Years" "30 - 39 Years" "40 - 49 Years"
		   "50 - 59 Years" "60 - 69 Years" "70 - 79 Years" "80+ Years"))

    ;; Initialize a compartment as used in the simulation.
    ;; The return value is a list of age-specific compartments (one for each age group).
    ;; Each age-specific is formatted as '(compartment-name age-group number-in-group).
    ;; An example age-specific compartment would be '(recovered "30 - 39 Years" 10000).
    (define (make-compartment c c-test age-groups db-hashed)
      (map (lambda (age) (list c age (count c-test (db-map age age-groups db-hashed)))) ages))

    ;; Return all compartments for the age group age-type in state.
    (define (filter-age state age-type)
      (filter (lambda (c) (equal? age-type (cadr c))) state))

    ;; Returns the sum of people in all compartments in state.
    (define (total-in-state state) (apply + (map caddr state)))

    ;; Size of age group within a specific state.
    (define (age-total age state) (total-in-state (filter-age state age)))

    ;; Population size of a given age.
    (define (age-pop age) (assoc-ref pop-by-age age))

    ;; Prints help prompt if the help flag was called in the shell.
    (when help-opt-flag (help-opt))

    ;; Generates the initial state of a simulation if
    ;; a CDC database was present as a shell argument.
    (when input-db
      (let* ((db-hashed (alist->hashq-table (cdr input-db)))
	     (db-legend                      (car input-db))
	     (age-groups    (assq-ref db-legend 'age_group))
	     (init-recovered  (make-compartment  'recovered  recovered? age-groups db-hashed))
	     (init-infectious (make-compartment 'infectious infectious? age-groups db-hashed))
	     (init-dead       (map (lambda (age) (list       'dead age 0)) ages))
	     (init-vaccinated (map (lambda (age) (list 'vaccinated age 0)) ages))
	     (immune (append init-infectious init-recovered init-vaccinated init-dead))
	     (init-susceptible ;; Susceptible population of age A is (- A-all A-immune).
	      (map (lambda (a) (list 'susceptible a (- (age-pop a) (age-total a immune)))) ages))
	     (init-state (append init-susceptible immune)))

	;; Output options.
	(when out-write-flag (write        init-state))
	(when out-print-flag (pretty-print init-state))
	(when out-file-flag
	  (with-output-to-file out-file-flag (lambda () (write init-state))))))))

(convert-cdc)
