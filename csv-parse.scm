(use-modules (ice-9 hash-table) (ice-9 rdelim))
(load "alist-db.scm")

(define (csv->alist ll)
  (let ((header-lst (map string->symbol (car ll))))
    (define (csv-row->alist-pair row)
      (cons (gensym "ht-entry-key-")
	    (map cons header-lst row)))
    (cons header-lst (map csv-row->alist-pair (cdr ll)))))

(define (csv-file->hash-table fp)
  (define (read-in-csv csv-lst)
    (let ((line-in (read-line fp)))
      (cond ((eof-object? line-in) (reverse csv-lst))
	    (else (read-in-csv (cons (string-split line-in #\,) csv-lst))))))
  (alist->hashq-table (read-in-csv '())))

(define (csv-file->csv-lst fp)
  (define (read-in-csv csv-lst)
    (let ((line-in (read-line fp)))
      (cond ((eof-object? line-in) (reverse csv-lst))
	    (else (read-in-csv (cons (string-split line-in #\,) csv-lst))))))
  (read-in-csv '()))

(define (csv->alist-db ll) (alist->alist-db (csv->alist ll)))

(define (model->csv-alt model)
  (define (state->csv state)
    (apply string-append
	   (map (lambda (compartment)
		  (string-append
		   (number->string (car         state))  ","
		   (symbol->string (car   compartment))  ","
		   (cadr  compartment)                   ","
		   (number->string (caddr compartment)) "\n"))
		(cdr state))))
  (apply string-append
	 (cons "time,compartment,age,total\n"
	       (map state->csv model))))

(define (model->csv model)
  (define ages '("0 - 9 Years"   "10 - 19 Years" "20 - 29 Years"
		 "30 - 39 Years" "40 - 49 Years" "50 - 59 Years"
		 "60 - 69 Years" "70 - 79 Years" "80+ Years"))
  (define (state->csv state)
    (apply string-append
	   (map (lambda (age)
		  (let ((compartments-of-age (filter (lambda (c) (equal? age (cadr c))) (cdr state))))
		    (string-append
		     (number->string (car state)) ","
		     age ","
		     (number->string (caddr (assq 'susceptible compartments-of-age))) ","
		     (number->string (caddr (assq 'vaccinated  compartments-of-age))) ","
		     (number->string (caddr (assq 'recovered   compartments-of-age))) ","
		     (number->string (caddr (assq 'infectious  compartments-of-age))) ","
		     (number->string (caddr (assq 'dead        compartments-of-age))) "\n")))
		ages)))
  (apply string-append
	 (cons "time,age,susceptible,vaccinated,recovered,infectious,dead\n"
	       (map state->csv model))))

(define (db-keys key alist-db)
  (append-map cdr (filter (lambda (alist-item) (equal? key (car alist-item))) alist-db)))

(define (db-map key alist-db hashed-db)
  (map (lambda (hash-key) (hashq-ref hashed-db hash-key)) (db-keys key alist-db)))

(define (infectious? db-entry)
  (and (string<=? "2020/07/12" (assq-ref db-entry 'cdc_report_dt))
       (not    (string=? "Yes" (assq-ref db-entry     'death_yn)))))

(define (recovered? db-entry)
  (and (string>? "2020/07/12" (assq-ref db-entry 'cdc_report_dt))
       (not   (string=? "Yes" (assq-ref db-entry     'death_yn)))))
